export const environment = {
    "name": "dev",
    "properties": {
        "production": false,
        "ssdURL": "http://localhost:8081/api/",
        "tenantName": "silverlake",
        "appName": "silverlakeaxisdemo",
        "namespace": "com.silverlake.silverlakeaxisdemo",
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "useDefaultExceptionUI": true,
        "isIDSEnabled": "true"
    }
}