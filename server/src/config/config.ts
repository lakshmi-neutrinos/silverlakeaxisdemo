export default {
  logger: { level: 'debug', transport: ['file', 'console'] },
  middlewares: {
    pre: [
      { ids: 'hrefstart' },
      { __ssdGlobalMiddlewares__: 'sd_7EmxxOk703exD5hF' },
      { __ssdGlobalMiddlewares__: 'sd_Tp9jNbUvUZ1kJnAn' }
    ],
    post: [],
    sequences: { IDSAuthroizedAPIs: { pre: [{ ids: 'Authorize' }], post: [] } }
  },
  ids: {
    client_id: 'ZzfxN3LFY-DDZ4fnn-3T2',
    client_secret:
      'ABpdBtLE1zP1wrSSRKB38Us7BOr5T-EOsq0KP0L1HsE6d4-h4hV-vOfLN0VqZ2zwrei6nRoy-gIfedQfS3l9Yw',
    issuerURL: 'https://ids.neutrinos.co',
    enabled: true
  }
};
