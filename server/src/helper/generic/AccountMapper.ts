export const ACCOUNT_MAPPER = {
    'neutrinos': {
        firstName: 'firstName',
        lastName: 'lastName',
        username: 'username',
        userKey: 'userKey',
        displayName: 'displayName',
        groupList: 'groupList',
        clients: 'clients'
    },
    'activeDirectory': {
        firstName: 'givenName',
        lastName: 'sn',
        username: 'sAMAccountName',
        userKey: 'userPrincipalName',
        displayName: 'displayName',
        groupList: 'groupList',
    },
    'azure': {
        firstName: 'given_name',
        lastName: 'family_name',
        username: 'email',
        email: 'email',
        displayName: 'name',
        unique_name: 'unique_name',
        groups: 'groupList',
    },
    'google': {
        firstName: 'given_name',
        lastName: 'family_name',
        username: 'email',
        email: 'email',
        displayName: 'name',
        groups: 'groupList',
    }
};