import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import { Parsers } from '../helper/parsers/parsers';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start
//append_imports_end
export class idsutil {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'idsutil';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new idsutil(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_idsutil_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: idsutil');

    //appendnew_flow_idsutil_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: idsutil');
    //appendnew_flow_idsutil_HttpIn
  }
  //   service flows_idsutil

  public async getIDSClientInstance(clientInstance = null, ...others) {
    let bh = { input: { clientInstance: clientInstance }, local: {} };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_betBtgeJMpwInMKJ(bh);
      //appendnew_next_getIDSClientInstance
      //Start formatting output variables
      let outputVariables = {
        input: { clientInstance: bh.input.clientInstance },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_cpwzq2OdhURjONdA');
    }
  }
  public async getAuthorizationParams(authParams = null, ...others) {
    let bh = { input: { authParams: authParams }, local: {} };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_uf7s7lQ9PTpccxJr(bh);
      //appendnew_next_getAuthorizationParams
      //Start formatting output variables
      let outputVariables = {
        input: { authParams: bh.input.authParams },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_eZ8OJH4TkXhrzaMq');
    }
  }
  public async handleTokenExpiry(
    existingSession = '',
    newSession = '',
    ...others
  ) {
    let bh = {
      input: { existingSession: existingSession, newSession: newSession },
      local: {}
    };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_5l2btlZk2fUCUy8P(bh);
      //appendnew_next_handleTokenExpiry
      //Start formatting output variables
      let outputVariables = {
        input: { newSession: bh.input.newSession },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_CtZOXZogGDRaEfiY');
    }
  }
  //appendnew_flow_idsutil_Start

  //new_service_variable_client
  client: any;
  async sd_betBtgeJMpwInMKJ(bh) {
    try {
      bh.local.client = this['client'];
      bh = await this.sd_uqkBE6pmZ3y713pL(bh);
      //appendnew_next_sd_betBtgeJMpwInMKJ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_betBtgeJMpwInMKJ');
    }
  }

  async sd_uqkBE6pmZ3y713pL(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['istype'](
          bh.local.client,
          'undefined',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_dPWuahG5uBEyVjGl(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_IgiXk4qNVim7WnEP(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_uqkBE6pmZ3y713pL');
    }
  }

  async sd_dPWuahG5uBEyVjGl(bh) {
    try {
      const DEFAULT_HTTP_OPTIONS = {
        timeout: 60000
      };

      custom.setHttpOptionsDefaults({
        timeout: DEFAULT_HTTP_OPTIONS.timeout
      });
      log.info(
        `Identity server default HTTP options : ${DEFAULT_HTTP_OPTIONS}`
      );
      const issuer = await Issuer.discover(
        settings.default['ids']['issuerURL']
      );
      log.info(`Identity server discovered at : ${issuer.issuer}`);
      const client = await new issuer.Client({
        client_id: settings.default['ids']['client_id'],
        client_secret: settings.default['ids']['client_secret']
      });
      client[custom.clock_tolerance] = process.env.CLOCK_TOLERANCE;
      log.info('Client connected...');
      bh.input.clientInstance = client;

      bh = await this.sd_VR4VhTMGCiQSfFcp(bh);
      //appendnew_next_sd_dPWuahG5uBEyVjGl
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_dPWuahG5uBEyVjGl');
    }
  }

  async sd_VR4VhTMGCiQSfFcp(bh) {
    try {
      this['client'] = bh.input.clientInstance;
      //appendnew_next_sd_VR4VhTMGCiQSfFcp
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VR4VhTMGCiQSfFcp');
    }
  }

  async sd_IgiXk4qNVim7WnEP(bh) {
    try {
      bh.input.clientInstance = this['client'];
      //appendnew_next_sd_IgiXk4qNVim7WnEP
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_IgiXk4qNVim7WnEP');
    }
  }
  async sd_uf7s7lQ9PTpccxJr(bh) {
    try {
      bh.input.authParams = {
        scope: 'openid profile email address phone offline_access user',
        prompt: 'consent'
      };

      //appendnew_next_sd_uf7s7lQ9PTpccxJr
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_uf7s7lQ9PTpccxJr');
    }
  }
  async sd_5l2btlZk2fUCUy8P(bh) {
    try {
      const tokenset = bh.input.existingSession.data.tokenset;
      bh.local.expires_at = tokenset['expires_at'] * 1000;
      bh.local.refreshTime = new Date().valueOf() + 300000; // 5min before

      bh = await this.sd_Iwtj3vPls56SKobf(bh);
      //appendnew_next_sd_5l2btlZk2fUCUy8P
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_5l2btlZk2fUCUy8P');
    }
  }

  async sd_Iwtj3vPls56SKobf(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['gt'](
          bh.local.expires_at,
          bh.local.refreshTime,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_XedsW4L3c6FLsIps(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_fX8a0y0c0SNvdA9o(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Iwtj3vPls56SKobf');
    }
  }

  async sd_XedsW4L3c6FLsIps(bh) {
    try {
      bh.input.newSession = bh.input.existingSession.data;

      //appendnew_next_sd_XedsW4L3c6FLsIps
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XedsW4L3c6FLsIps');
    }
  }
  async sd_fX8a0y0c0SNvdA9o(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_jVTrePlLt3aRHZth(bh);
      //appendnew_next_sd_fX8a0y0c0SNvdA9o
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_fX8a0y0c0SNvdA9o');
    }
  }
  async sd_jVTrePlLt3aRHZth(bh) {
    try {
      bh.local.refresh_token = await bh.input.client.introspect(
        bh.input.existingSession.data.tokenset.refresh_token,
        'refresh_token'
      );

      bh = await this.sd_SA04FkbDKvFrOhdE(bh);
      //appendnew_next_sd_jVTrePlLt3aRHZth
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_jVTrePlLt3aRHZth');
    }
  }

  async sd_SA04FkbDKvFrOhdE(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.refresh_token.active,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_15DoU77DIXI7YH8E(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_JKDu8D1C6Dgo1Fj6(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_SA04FkbDKvFrOhdE');
    }
  }

  async sd_15DoU77DIXI7YH8E(bh) {
    try {
      bh.input.newSession = { rotated: true };
      bh.input.newSession['tokenset'] = await bh.input.client.refresh(
        bh.input.existingSession.data.tokenset.refresh_token
      );
      bh.input.newSession['userInfo'] = await bh.input.client.userinfo(
        bh.input.newSession['tokenset']['access_token']
      );
      bh.input.newSession['tokenset']['claims'] = Object.assign(
        {},
        bh.input.newSession['tokenset'].claims()
      );

      //appendnew_next_sd_15DoU77DIXI7YH8E
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_15DoU77DIXI7YH8E');
    }
  }
  async sd_JKDu8D1C6Dgo1Fj6(bh) {
    try {
      bh.input.newSession = false;

      //appendnew_next_sd_JKDu8D1C6Dgo1Fj6
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_JKDu8D1C6Dgo1Fj6');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_YvJKTJzlKY5kZL6z(bh)) /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }

  async sd_YvJKTJzlKY5kZL6z(bh) {
    const nodes = ['handleTokenExpiry'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_JKDu8D1C6Dgo1Fj6(bh);
      //appendnew_next_sd_YvJKTJzlKY5kZL6z
      return true;
    }
    return false;
  }

  //appendnew_flow_idsutil_Catch
}
