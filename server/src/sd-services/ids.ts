import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import { Parsers } from '../helper/parsers/parsers';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { idsutil } from './idsutil'; //_splitter_
//append_imports_end

export class ids {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'ids';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new ids(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_ids_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: ids');

    let mw_hrefstart: Middleware = new Middleware(
      this.serviceName,
      'hrefstart',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_D3V1Ky6ibsFHpGeR(bh);
          //appendnew_next_mw_hrefstart
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_9eBoGiJ2di7QUClH');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['hrefstart'] = mw_hrefstart;

    let mw_Authorize: Middleware = new Middleware(
      this.serviceName,
      'Authorize',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_rcspYy0E1naYhQ4t(bh);
          //appendnew_next_mw_Authorize
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_Pe17o1YVHoyctw0Q');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['Authorize'] = mw_Authorize;

    //appendnew_flow_ids_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: ids');

    this.swaggerDocument['paths']['/login'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/login`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_GYReCfNSLyTdZUO4(bh);
          //appendnew_next_sd_NgyV4mgr6luw7sDL
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_NgyV4mgr6luw7sDL');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/login/cb'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/login/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_Z9mO4uFWvBEqMeGl(bh);
          //appendnew_next_sd_IkUmR6j65UZvH6Fz
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_IkUmR6j65UZvH6Fz');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/user/info'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/user/info`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_s3GVvaFZITiOHHXE(bh);
          //appendnew_next_sd_cY4GsEyD3utsDx28
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_cY4GsEyD3utsDx28');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/logout'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/logout`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_bSg9q3WGSLX06C9O(bh);
          //appendnew_next_sd_0lnx2R5MEWKRwVxp
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_0lnx2R5MEWKRwVxp');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/logout/cb'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/logout/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_5cWNHk6LUQepti8f(bh);
          //appendnew_next_sd_i5ldG35VLmSeoN16
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_i5ldG35VLmSeoN16');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_ids_HttpIn
  }
  //   service flows_ids

  //appendnew_flow_ids_Start

  async sd_GYReCfNSLyTdZUO4(bh) {
    try {
      bh.local.idsConfigured = false;
      if (
        settings.default.hasOwnProperty('ids') &&
        settings.default['ids'].hasOwnProperty('client_id') &&
        settings.default['ids'].hasOwnProperty('client_secret')
      ) {
        bh.local.idsConfigured = true;
      }

      bh = await this.sd_fYekM3CKSudSrbf8(bh);
      //appendnew_next_sd_GYReCfNSLyTdZUO4
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_GYReCfNSLyTdZUO4');
    }
  }

  async sd_fYekM3CKSudSrbf8(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.idsConfigured,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_0UGtTD8CPsgXxRWI(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_SwVFUAJGTadH0FlW(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_fYekM3CKSudSrbf8');
    }
  }

  async sd_0UGtTD8CPsgXxRWI(bh) {
    try {
      bh.local.reqParams = {
        state: crypto.randomBytes(16).toString('hex'),
        nonce: crypto.randomBytes(16).toString('hex'),
        isMobile: bh.input.query.isMobile,
        redirectTo: bh.input.query.redirectTo
      };

      bh = await this.sd_SqXyiuJVdL6io8Ds(bh);
      //appendnew_next_sd_0UGtTD8CPsgXxRWI
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0UGtTD8CPsgXxRWI');
    }
  }

  async sd_SqXyiuJVdL6io8Ds(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.reqParams;
    }
    bh = await this.sd_FHilqIBYZfdJFPDF(bh);
    //appendnew_next_sd_SqXyiuJVdL6io8Ds
    return bh;
  }

  async sd_FHilqIBYZfdJFPDF(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_Ptf3sbKZ1fJOg42A(bh);
      //appendnew_next_sd_FHilqIBYZfdJFPDF
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_FHilqIBYZfdJFPDF');
    }
  }
  async sd_Ptf3sbKZ1fJOg42A(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getAuthorizationParams();
      bh.input.authParams = outputVariables.input.authParams;

      bh = await this.sd_H2yzh9L2pwRid6gg(bh);
      //appendnew_next_sd_Ptf3sbKZ1fJOg42A
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Ptf3sbKZ1fJOg42A');
    }
  }
  async sd_H2yzh9L2pwRid6gg(bh) {
    try {
      const authorizationRequest = Object.assign(
        {
          redirect_uri: url.resolve(bh.web.req.href, '/api/login/cb'),
          scope: 'openid profile email address phone user',
          state: bh.local.reqParams.state,
          nonce: bh.local.reqParams.nonce,
          response_type: bh.input.client.response_types[0]
        },
        bh.input.authParams
      );

      bh.local.redirectHeaders = {
        location: bh.input.client.authorizationUrl(authorizationRequest)
      };

      await this.sd_zXIcsQhFGVbe1RiG(bh);
      //appendnew_next_sd_H2yzh9L2pwRid6gg
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_H2yzh9L2pwRid6gg');
    }
  }
  async sd_zXIcsQhFGVbe1RiG(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zXIcsQhFGVbe1RiG');
    }
  }
  async sd_SwVFUAJGTadH0FlW(bh) {
    try {
      bh.local.res = {
        message:
          'IDS client not registered. Register on the Neutrinos Stuido and try again'
      };

      await this.sd_UMZiYtj6kvTqEw6g(bh);
      //appendnew_next_sd_SwVFUAJGTadH0FlW
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_SwVFUAJGTadH0FlW');
    }
  }
  async sd_UMZiYtj6kvTqEw6g(bh) {
    try {
      bh.web.res.status(404).send(bh.local.res.message);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_UMZiYtj6kvTqEw6g');
    }
  }
  async sd_D3V1Ky6ibsFHpGeR(bh) {
    try {
      const protocol =
        bh.input.headers['x-forwarded-proto'] || bh.web.req.protocol;
      const href =
        protocol + '://' + bh.web.req.get('Host') + bh.web.req.originalUrl;
      bh.web.req.href = href;

      await this.sd_8cn7Fo9h5rSMbrUg(bh);
      //appendnew_next_sd_D3V1Ky6ibsFHpGeR
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_D3V1Ky6ibsFHpGeR');
    }
  }
  async sd_8cn7Fo9h5rSMbrUg(bh) {
    try {
      bh.web.next();

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8cn7Fo9h5rSMbrUg');
    }
  }

  async sd_Z9mO4uFWvBEqMeGl(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.input.sessionParams = JSON.parse(
        JSON.stringify(requestObject.session)
      );
    }
    bh = await this.sd_yPIdauTABf18zRwW(bh);
    //appendnew_next_sd_Z9mO4uFWvBEqMeGl
    return bh;
  }

  async sd_yPIdauTABf18zRwW(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_ga5kLOfDr6Xa9Fqx(bh);
      //appendnew_next_sd_yPIdauTABf18zRwW
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_yPIdauTABf18zRwW');
    }
  }
  async sd_ga5kLOfDr6Xa9Fqx(bh) {
    try {
      const params = bh.input.client.callbackParams(bh.web.req);
      let tokenset = await bh.input.client.callback(
        url.resolve(bh.web.req.href, 'cb'),
        params,
        {
          nonce: bh.input.sessionParams.data.nonce,
          state: bh.input.sessionParams.data.state
        }
      );

      bh.local.redirectTo = bh.input.sessionParams.data.redirectTo;

      bh.local.userDetails = {
        tokenset: Object.assign({}, tokenset),
        userInfo: await bh.input.client.userinfo(tokenset['access_token'])
      };
      bh.local.userDetails['tokenset']['claims'] = Object.assign(
        {},
        tokenset.claims()
      );

      bh = await this.sd_WxPGscVcqO3icKJg(bh);
      //appendnew_next_sd_ga5kLOfDr6Xa9Fqx
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ga5kLOfDr6Xa9Fqx');
    }
  }

  async sd_WxPGscVcqO3icKJg(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.userDetails;
    }
    bh = await this.sd_EcI4x3Z3MIKqwyQH(bh);
    //appendnew_next_sd_WxPGscVcqO3icKJg
    return bh;
  }

  async sd_EcI4x3Z3MIKqwyQH(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.input.sessionParams.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_Lqp3I1FdVGOpuDQM(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_LvycfThzTe84Yw5U(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_EcI4x3Z3MIKqwyQH');
    }
  }

  async sd_Lqp3I1FdVGOpuDQM(bh) {
    try {
      bh.local.htmlResponse = `
 <html>
   <script>
      let _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;

      await this.sd_ENHAy57NDjJ6JZtk(bh);
      //appendnew_next_sd_Lqp3I1FdVGOpuDQM
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Lqp3I1FdVGOpuDQM');
    }
  }
  async sd_ENHAy57NDjJ6JZtk(bh) {
    try {
      bh.web.res.status(200).send(bh.local.htmlResponse);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ENHAy57NDjJ6JZtk');
    }
  }
  async sd_LvycfThzTe84Yw5U(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.redirectTo
      };

      await this.sd_h5NBy7TZ3jHuzLU3(bh);
      //appendnew_next_sd_LvycfThzTe84Yw5U
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_LvycfThzTe84Yw5U');
    }
  }
  async sd_h5NBy7TZ3jHuzLU3(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('Redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_h5NBy7TZ3jHuzLU3');
    }
  }

  async sd_s3GVvaFZITiOHHXE(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.session = JSON.parse(JSON.stringify(requestObject.session));
    }
    await this.sd_Kivbp3QjhlO55Ya4(bh);
    //appendnew_next_sd_s3GVvaFZITiOHHXE
    return bh;
  }

  async sd_Kivbp3QjhlO55Ya4(bh) {
    try {
      bh.web.res.status(200).send(bh.local.session.data.userInfo);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Kivbp3QjhlO55Ya4');
    }
  }
  async sd_YCBOnQtRHkMYnb2q(bh) {
    try {
      bh.web.res.redirect('/api/login');

      //appendnew_next_sd_YCBOnQtRHkMYnb2q
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_YCBOnQtRHkMYnb2q');
    }
  }

  async sd_bSg9q3WGSLX06C9O(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_VEPDKKiUB7ZC812T(bh);
    //appendnew_next_sd_bSg9q3WGSLX06C9O
    return bh;
  }

  async sd_VEPDKKiUB7ZC812T(bh) {
    try {
      bh.local.sessionExists = false;
      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset
      ) {
        bh.local.sessionData['data']['redirectTo'] =
          bh.input.query['redirectTo'];
        bh.local.sessionData['data']['isMobile'] = bh.input.query['isMobile'];
        bh.local.sessionExists = true;
      } else {
        delete bh.local.sessionData['redirectTo'];
      }

      bh = await this.sd_SYHhpR0Wnh86NzyU(bh);
      //appendnew_next_sd_VEPDKKiUB7ZC812T
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VEPDKKiUB7ZC812T');
    }
  }

  async sd_SYHhpR0Wnh86NzyU(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.sessionData.data;
    }
    bh = await this.sd_EJ1LkWNV2l0FBd2T(bh);
    //appendnew_next_sd_SYHhpR0Wnh86NzyU
    return bh;
  }

  async sd_EJ1LkWNV2l0FBd2T(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_jPhioW9nsOeipXyu(bh);
      //appendnew_next_sd_EJ1LkWNV2l0FBd2T
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_EJ1LkWNV2l0FBd2T');
    }
  }

  async sd_jPhioW9nsOeipXyu(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_mN1Bu4UvfsVxh7WO(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_LNRq454YPLDn27pL(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_jPhioW9nsOeipXyu');
    }
  }

  async sd_mN1Bu4UvfsVxh7WO(bh) {
    try {
      await Promise.all([
        bh.local.sessionData.data.tokenset.access_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.access_token,
              'access_token'
            )
          : undefined,
        bh.local.sessionData.data.tokenset.refresh_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.refresh_token,
              'refresh_token'
            )
          : undefined
      ]);

      bh.local.res = {
        idsURL: url.format(
          Object.assign(
            url.parse(bh.input.client.issuer.end_session_endpoint),
            {
              search: null,
              query: {
                id_token_hint: bh.local.sessionData.data.tokenset.id_token,
                post_logout_redirect_uri: url.resolve(
                  bh.web.req.href,
                  '/api/logout/cb'
                ),
                client_id: settings.default['ids']['client_id']
              }
            }
          )
        ),
        sessionExists: true
      };

      await this.sd_uBbohcjktxfcUJca(bh);
      //appendnew_next_sd_mN1Bu4UvfsVxh7WO
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_mN1Bu4UvfsVxh7WO');
    }
  }
  async sd_uBbohcjktxfcUJca(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_uBbohcjktxfcUJca');
    }
  }
  async sd_LNRq454YPLDn27pL(bh) {
    try {
      bh.local.res = {
        sessionExists: false
      };

      await this.sd_uBbohcjktxfcUJca(bh);
      //appendnew_next_sd_LNRq454YPLDn27pL
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_LNRq454YPLDn27pL');
    }
  }

  async sd_5cWNHk6LUQepti8f(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_3adF0FVhLMxg86BP(bh);
    //appendnew_next_sd_5cWNHk6LUQepti8f
    return bh;
  }

  async sd_3adF0FVhLMxg86BP(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      let p = function() {
        return new Promise((resolve, reject) => {
          requestObject.session.destroy(function(error) {
            if (error) {
              return reject(error);
            }
            return resolve();
          });
        });
      };
      try {
        await p();
        bh = await this.sd_SquM9tYzOq0jo9cf(bh);
        //appendnew_next_sd_3adF0FVhLMxg86BP
      } catch (e) {
        return await this.errorHandler(bh, e, 'sd_3adF0FVhLMxg86BP');
      }
      return bh;
    }
  }

  async sd_SquM9tYzOq0jo9cf(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.local.sessionData.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_O87wtlplNgiYY4ok(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_oxKck6kn2kplauA8(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_SquM9tYzOq0jo9cf');
    }
  }

  async sd_O87wtlplNgiYY4ok(bh) {
    try {
      bh.local.res = `<html>
   <script>
      var _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;

      await this.sd_zvnfSTuEFqtMxbCO(bh);
      //appendnew_next_sd_O87wtlplNgiYY4ok
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_O87wtlplNgiYY4ok');
    }
  }
  async sd_zvnfSTuEFqtMxbCO(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zvnfSTuEFqtMxbCO');
    }
  }
  async sd_oxKck6kn2kplauA8(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.sessionData.data.redirectTo
      };

      await this.sd_XKRzbtkRTUOI9Ddf(bh);
      //appendnew_next_sd_oxKck6kn2kplauA8
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_oxKck6kn2kplauA8');
    }
  }
  async sd_XKRzbtkRTUOI9Ddf(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XKRzbtkRTUOI9Ddf');
    }
  }
  async sd_rcspYy0E1naYhQ4t(bh) {
    try {
      bh.local = {};

      bh = await this.sd_pTvW7ruTUEt0Uw3Q(bh);
      //appendnew_next_sd_rcspYy0E1naYhQ4t
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rcspYy0E1naYhQ4t');
    }
  }

  async sd_pTvW7ruTUEt0Uw3Q(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_UcOoqHLoVzaprCvB(bh);
    //appendnew_next_sd_pTvW7ruTUEt0Uw3Q
    return bh;
  }

  async sd_UcOoqHLoVzaprCvB(bh) {
    try {
      bh.local.sessionExists = false;

      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset &&
        bh.local.sessionData.data.tokenset.access_token &&
        bh.local.sessionData.data.tokenset.refresh_token
      ) {
        bh.local.sessionExists = true;
      }

      bh = await this.sd_fClDgldTaf1NVkeD(bh);
      //appendnew_next_sd_UcOoqHLoVzaprCvB
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_UcOoqHLoVzaprCvB');
    }
  }

  async sd_fClDgldTaf1NVkeD(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_gbSHFYjqb5yREDMh(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_fg6gRDhVT4YSqE0t(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_fClDgldTaf1NVkeD');
    }
  }

  async sd_gbSHFYjqb5yREDMh(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.handleTokenExpiry(
        bh.local.sessionData,
        null
      );
      bh.local.newSession = outputVariables.input.newSession;

      bh = await this.sd_N8LTsd9GgvHCt5OO(bh);
      //appendnew_next_sd_gbSHFYjqb5yREDMh
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_gbSHFYjqb5yREDMh');
    }
  }

  async sd_N8LTsd9GgvHCt5OO(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['false'](
          bh.local.newSession,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_b3Ejb2RcrG8T6Wdy(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_4hvxqqdAlsAtLBlx(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_N8LTsd9GgvHCt5OO');
    }
  }

  async sd_b3Ejb2RcrG8T6Wdy(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      let p = function() {
        return new Promise((resolve, reject) => {
          requestObject.session.destroy(function(error) {
            if (error) {
              return reject(error);
            }
            return resolve();
          });
        });
      };
      try {
        await p();
        bh = await this.sd_tftbuf2bc8HcdhxH(bh);
        //appendnew_next_sd_b3Ejb2RcrG8T6Wdy
      } catch (e) {
        return await this.errorHandler(bh, e, 'sd_b3Ejb2RcrG8T6Wdy');
      }
      return bh;
    }
  }

  async sd_tftbuf2bc8HcdhxH(bh) {
    try {
      bh.local.res = {
        code: 'TOKEN_EXPIRED',
        message: 'Token invalid or access revoked'
      };

      await this.sd_Bop3sCaLlBeKJERP(bh);
      //appendnew_next_sd_tftbuf2bc8HcdhxH
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_tftbuf2bc8HcdhxH');
    }
  }
  async sd_Bop3sCaLlBeKJERP(bh) {
    try {
      bh.web.res.status(403).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Bop3sCaLlBeKJERP');
    }
  }

  async sd_4hvxqqdAlsAtLBlx(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.newSession.rotated,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_lHJ050KBZI3Mpj27(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_HkX43hjaeWC40Lwv(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_4hvxqqdAlsAtLBlx');
    }
  }

  async sd_lHJ050KBZI3Mpj27(bh) {
    try {
      delete bh.local.newSession.rotated;

      bh = await this.sd_PAJsRaBvbVMRwhKx(bh);
      //appendnew_next_sd_lHJ050KBZI3Mpj27
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_lHJ050KBZI3Mpj27');
    }
  }

  async sd_PAJsRaBvbVMRwhKx(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.newSession;
    }
    await this.sd_HkX43hjaeWC40Lwv(bh);
    //appendnew_next_sd_PAJsRaBvbVMRwhKx
    return bh;
  }

  async sd_HkX43hjaeWC40Lwv(bh) {
    try {
      bh.web.next();

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_HkX43hjaeWC40Lwv');
    }
  }

  async sd_fg6gRDhVT4YSqE0t(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['cont'](
          bh.input.path,
          '/user/info',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_aa3lpYjwcT9uBQUV(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_EKP0G0yTiu3QLJiG(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_fg6gRDhVT4YSqE0t');
    }
  }

  async sd_aa3lpYjwcT9uBQUV(bh) {
    try {
      bh.local.res = { message: 'Session expired' };

      await this.sd_Bop3sCaLlBeKJERP(bh);
      //appendnew_next_sd_aa3lpYjwcT9uBQUV
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_aa3lpYjwcT9uBQUV');
    }
  }
  async sd_EKP0G0yTiu3QLJiG(bh) {
    try {
      bh.local.res = { code: 'NO_SESSION', message: 'Session not present' };

      await this.sd_Bop3sCaLlBeKJERP(bh);
      //appendnew_next_sd_EKP0G0yTiu3QLJiG
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_EKP0G0yTiu3QLJiG');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_HToakyP6Zx4pRtrV(bh)) ||
      (await this.sd_4r5epdJPe8RfqB3D(bh)) /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }

  async sd_HToakyP6Zx4pRtrV(bh) {
    const nodes = [
      'sd_Ptf3sbKZ1fJOg42A',
      'sd_IkUmR6j65UZvH6Fz',
      'sd_yPIdauTABf18zRwW',
      'sd_ga5kLOfDr6Xa9Fqx',
      'sd_Z9mO4uFWvBEqMeGl',
      'sd_EcI4x3Z3MIKqwyQH',
      'sd_Lqp3I1FdVGOpuDQM',
      'sd_LvycfThzTe84Yw5U',
      'sd_ENHAy57NDjJ6JZtk',
      'sd_h5NBy7TZ3jHuzLU3'
    ];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_YCBOnQtRHkMYnb2q(bh);
      //appendnew_next_sd_HToakyP6Zx4pRtrV
      return true;
    }
    return false;
  }

  async sd_4r5epdJPe8RfqB3D(bh) {
    const nodes = ['sd_gbSHFYjqb5yREDMh'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_tftbuf2bc8HcdhxH(bh);
      //appendnew_next_sd_4r5epdJPe8RfqB3D
      return true;
    }
    return false;
  }

  //appendnew_flow_ids_Catch
}
